#!/bin/bash
# Create by Sam Gleske
# Sun Jun 10 20:46:20 PDT 2018
# Linux 4.10.0-28-generic x86_64
# GNU bash, version 4.3.48(1)-release (x86_64-pc-linux-gnu)

# DESCRIPTION:
#     Build GIMP and all prerequisites.

(
    # cd in a subshell so we don't change directory for the build process
    cd "${0%/*}/"
    # build everything
    cat babl.sh gegl.sh libmypaint.sh mypaint-brushes.sh gimp.sh
) | /bin/bash
